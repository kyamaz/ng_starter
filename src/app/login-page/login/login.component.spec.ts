import { async, ComponentFixture, TestBed,inject } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import {HttpClientModule} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {RouterModule, ActivatedRoute, Router} from "@angular/router";

import { FormGroup, FormControl, FormBuilder,  Validators, ReactiveFormsModule  } from '@angular/forms';
import { MatInputModule  } from '@angular/material';
import { AuthService } from './../../shared/services/auth/auth.service';
import {WindowRef} from './../../window_ref';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {SharedComponentModule} from './../../ng-starter-app/components/commons/shared_component.module';
import {NotifyResponseService} from './../../shared/services/notify-response.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports:[
        MatInputModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterTestingModule,
        SharedComponentModule,
        BrowserAnimationsModule
        
        ],
      providers:[
        AuthService,
        WindowRef,
        MatDialog,
        NotifyResponseService,
        {
          provide:ActivatedRoute
        },
        {
          provide:Router
        },
        
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });



});
