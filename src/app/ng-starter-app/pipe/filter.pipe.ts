import { Pipe, PipeTransform,Injectable } from '@angular/core';
import {FormControl} from '@angular/forms';

@Injectable()
@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: Array<any>, category: Object): Array<any> {
    if (items && category) {
        const cat=category['cat']
        if(!!!category['cat']){
            return items;
        }
        return items.filter(item => item[cat].toUpperCase() ===category['slug'].toUpperCase());
        
        } else {
            return items        
        }
    }
}


