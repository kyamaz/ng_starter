import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RedirectComponent } from './redirect.component';
const routes: Routes = [
    {path:'', component:RedirectComponent},    
    { path: '**',redirectTo: '',pathMatch: 'full'},
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);