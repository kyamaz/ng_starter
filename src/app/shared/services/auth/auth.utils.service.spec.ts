import { TestBed, inject } from '@angular/core/testing';

import { AuthUtilsService } from './auth.utils.service';

describe('Auth.UtilsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthUtilsService]
    });
  });

  it('should be created', inject([AuthUtilsService], (service: AuthUtilsService) => {
    expect(service).toBeTruthy();
  }));
});
