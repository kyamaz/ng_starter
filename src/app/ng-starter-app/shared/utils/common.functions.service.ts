import { Injectable } from '@angular/core';
/* import  _compact  = require('lodash/compact'); 
import _cloneDeep = require('lodash/cloneDeep'); */
import _compact from 'lodash/fp/compact';
import _cloneDeep from 'lodash/fp/cloneDeep';
export { _cloneDeep, _compact };


@Injectable()
export class CommonFunctionsService{}
