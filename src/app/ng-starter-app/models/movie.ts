import { MovieResults } from './movie';
import { List } from './commons';
export interface SearchMovie {
    search:string,
    type?:string,
    year?:number
}
export const init_search_form:SearchMovie={
    search:undefined,
    type:undefined,
    year:undefined
}
export const form_types:Array<List>=[
    {
      slug:"Movie",
      viewValue:"Movie"
    },
    {
      slug:"Series",
      viewValue:"Series"
    },
    {
      slug:"Episode",
      viewValue:"Episode"
    },
    {
      slug:"Game",
      viewValue:"Game",
    },
    {
      slug: undefined,
      viewValue:"All",
    }
  ];

 interface MovieInfo{ 
    Title: string,
    Year: string,
    imdbID:string,
    Type: string,
    Poster: string        
  }
  export interface MovieResults{
    Search: Array<MovieInfo>,
    totalResults: string,
    Response: string
  }
export const movie_res_headers:Array<List>=[
    {
      slug:"Title",
      viewValue:"Title"
    },
    {
      slug:"Type",
      viewValue:"Type"
    },
    {
      slug:"Year",
      viewValue:"Year"
    }
  ];

 export interface ResType extends List{
    cat:string | undefined
 }
  export const res_types:Array<ResType>=[
    {
      slug:"Movie",
      viewValue:"Movie",
      cat:'Type'
    },
    {
      slug:"Series",
      viewValue:"Series",
      cat:'Type'
    },
    {
      slug:"Episode",
      viewValue:"Episode",
      cat:'Type'
    },
    {
      slug:"Game",
      viewValue:"Game",
      cat:'Type'
    },
    {
      slug:"All",
      viewValue:"All",
      cat: undefined
    }
  ]
     
 export  interface MovieExt extends MovieInfo{
    Rated: string,
    Released: string,
    Runtime: string,
    Genre: string,
    Director: string,
    Writer: string,
    Actors: string,
    Plot: string,
    Language: string,
    Country: string,
    Awards: string,
    Ratings: Array<Rating>,
    Metascore: string,
    imdbRating: string,
    imdbVotes: string,
    DVD: string,
    BoxOffice: string,
    Production: string,
    Website: string,
    Response: string
}
interface Rating{
  Source: string,
  Value: string
}
