import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {NotifyResponseService} from "./../../../../shared/services/notify-response.service";

@Component({
  selector: 'server-response',
  templateUrl: './server-response.component.html',
  styleUrls: ['./server-response.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush

})
export class ServerResponseComponent  {
  @Input() msg:Object;
  public server$=new BehaviorSubject ({});  
  
  constructor( private _notifyResponseService :NotifyResponseService ) { }
  dismiss_notif(){
    this._notifyResponseService.server_response$.next({});
  }

}
