import { CanActivate, Router,ActivatedRoute , RouterStateSnapshot, CanDeactivate, ActivatedRouteSnapshot } from '@angular/router';
import {Injectable, Inject, PLATFORM_ID} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { SearchComponent } from '../../ng-starter-app/components/pages/movie/search.component';
import {WindowRef} from './../../window_ref'; 
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import { AuthService } from '../services/auth/auth.service';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Injectable()
export class RouteGuard implements CanActivate {
    private authorized;
    private has_auth:boolean=false;
    private has_logged:boolean=false;
    private check_auth$= new BehaviorSubject([])
    constructor( private _router:Router, 
    private winRef: WindowRef,
    private http:HttpClient,
    private _activatedRoute:ActivatedRoute,
    private _authService:AuthService,
    @Inject(PLATFORM_ID) private platformId: Object
 ){}

  canActivate():boolean| Observable<boolean>{
    this._authService.is_authorized$.subscribe(data=>this.authorized= data);
    //angular universal 
    if (isPlatformBrowser(this.platformId)) {
        if ( window !== undefined) {
            this.has_logged=!!this.winRef.nativeWindow.localStorage.getItem('session_token');
            if(this.has_logged){
                this._authService.checkAuthentication().subscribe();
                return true;
            }else{
                if(!!this.authorized.session_token){
                    this.winRef.nativeWindow.localStorage.setItem('session_token', this.authorized.session_token);
                    return true;
                }
            }
            this._router.navigate(['login']);
            return false;
        }
        this._router.navigate(['login']);
        return false;
     }       
  }

}
export class LeaveAppGuard implements CanDeactivate<SearchComponent> {
    constructor( ){}
    canDeactivate( component: SearchComponent){
    return  component.logOut()
    }
}

