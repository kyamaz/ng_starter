
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatInputModule, MatPaginatorModule,MatExpansionModule  } from '@angular/material';
//directive
import { PipesModule } from '../../pipe/pipes.module';
import { DirectivesModule } from './../../directives/directives.module';
//components
import { ServerResponseComponent } from './server-response/server-response.component';
import { ListHeaderComponent } from './list-header/list-header.component';
import { ListActionsComponent } from './list-actions/list-actions.component';

@NgModule({
  imports: [
     RouterModule,
     CommonModule,
     FormsModule, 
     ReactiveFormsModule,
     DirectivesModule,
     PipesModule,
     MatSelectModule,
     MatInputModule, 
     MatExpansionModule,
     MatPaginatorModule,
      ],
  declarations: [
    ServerResponseComponent,
    ListHeaderComponent,
    ListActionsComponent,
    ],
    exports: [ 
    RouterModule,
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    DirectivesModule,
    PipesModule,
    MatSelectModule,
    MatInputModule,     
    MatPaginatorModule,
    MatExpansionModule,
    ServerResponseComponent,
    ListHeaderComponent,
    ListActionsComponent
   
  ],

    })
export class SharedComponentModule {

 }