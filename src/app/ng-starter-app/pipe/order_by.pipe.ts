import {Pipe, PipeTransform, Injectable} from '@angular/core'; 
@Pipe({
    name: 'orderBy'
})
@Injectable()
export class OrderByPipe implements PipeTransform {
    transform(items: Array<any>, prop : string, order:boolean): any[] { 
    if(!!prop){
            let don_arr = items.map(i=>{
                return {
                    prop,
                    value: i
                };
            })
            don_arr.sort(ext_compare);
            let z = don_arr.map(i=>{
                return i['value'];
            })
            if(order){
                return z;
            }else{
                return z.reverse();
            }
        }else{
            return items
        }

    }
}

function ext_compare(a, b)
{
    let prop = null;
    if (!!a)
    {
        prop = a['prop']
    }
    else{
        return 1;
    }
    if (!b)
    {
        return -1;
    }
    let ia = a['value'];
    let ib = b['value'];
    let a_val = ia[prop];
    let b_val = ib[prop];
    return comparator(a_val, b_val);
}

function sub_comp_int(a, b){
    var reg_num = /^(\d+)$/g;
    var match_a = reg_num.exec(a);
    reg_num.lastIndex = 0;
    var match_b = reg_num.exec(b);
    if (match_a && match_b){
        return parseInt(a) - parseInt(b)}
    else if (match_a && !match_b)
        return -1;
    else if (match_b && !match_a)
        return 1;
    else
        return null;
}

function sub_comp_str_num(a, b){
    var reg_str_num = /^([^0-9]+)(\d*)$/;
    var match_a = reg_str_num.exec(a);
    reg_str_num.lastIndex = 0;
    var match_b = reg_str_num.exec(b);
    if (match_a && match_b){
        let anum = match_a[2];
        let atext = match_a[1];
        let bnum = match_b[2];
        let btext = match_b[1]
        if (atext == btext)
        {
            if (!bnum)
                return 1;
            if (!anum)
                return -1;
            return parseInt(anum) - parseInt(bnum);
        }
        return atext.localeCompare(btext)}
    else if (match_a && !match_b)
        return -1;
    else if (match_b && !match_a)
        return 1;
    else
        return null;
}

function sub_comp_num_str(a, b){
    var reg_num_str = /^(\d+)([^0-9]+)(\d*)/;
    var match_a = reg_num_str.exec(a);
    reg_num_str.lastIndex = 0;
    var match_b = reg_num_str.exec(b);
    if (match_a && match_b){
        let anum = match_a[1];
        let atext = match_a[2];
        let bnum = match_b[1];
        let btext = match_b[2]
        if (atext == btext)
            {
                if (anum != bnum)
                    return parseInt(anum) - parseInt(bnum);
                let a_num_2 = match_a[3];
                let b_num_2 = match_b[3];
                if (!b_num_2)
                    return 1;
                if (!a_num_2)
                    return -1;
                return parseInt(a_num_2) - parseInt(b_num_2);

            }

        return atext.localeCompare(btext)}
    else if (match_a && !match_b)
        return -1;
    else if (match_b && !match_a)
        return 1;
    else
        return null;
}

function comparator(a, b)
{
    let int_match = sub_comp_int(a, b)
    if (int_match != null)
        return int_match;

    let str_num_match = sub_comp_str_num(a, b)
    if (str_num_match != null)
        return str_num_match;

    let num_str_match = sub_comp_num_str(a, b)
    if (num_str_match != null)
        return num_str_match;
    return a.localeCompare(b);

}
