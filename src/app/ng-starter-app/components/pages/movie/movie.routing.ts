import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search.component';
import {MovieComponent} from './movie-page/movie.component';
const routes: Routes = [
    {path:'', component:SearchComponent},  
    {path:':id', component:MovieComponent},  
    { path: '**',redirectTo: '',pathMatch: 'full'},
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);