import { TestBed, async,fakeAsync, tick } from '@angular/core/testing';

import { AppComponent } from './app.component';
import {SharedComponentModule} from './ng-starter-app/components/commons/shared_component.module';
import { RouterTestingModule } from '@angular/router/testing';
import {Router, RouterOutlet, ActivatedRoute, ChildrenOutletContexts, RouterModule} from '@angular/router';
import {WindowRef} from './window_ref';
import {CommonModule, Location} from '@angular/common';
describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports:[
        CommonModule,
        RouterTestingModule,
        SharedComponentModule
            ],
      providers: [
        WindowRef,
        Location,
        {provide:ActivatedRoute},
        {provide: Router }
    ],

    }).compileComponents();

  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));


});
