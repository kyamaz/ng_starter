import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatInputModule} from '@angular/material';
import {NotifyResponseService} from '../shared/services/notify-response.service';

@NgModule({
    imports: [
    CommonModule,
     FormsModule, 
     ReactiveFormsModule,
     MatInputModule
    ],
  declarations: [
    LoginComponent  ],
  providers:[
    NotifyResponseService
  ],


})
export class LoginModule { }