import { Injectable } from '@angular/core';
import {WindowRef} from '../../../window_ref';   
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
@Injectable()
export class AuthUtilsService{
 constructor( private winRef :WindowRef){}
 setHeaderClient(request: HttpRequest<any>, payload = "", content_type: string = ""):HttpRequest<any>{
    request = request.clone({
      setHeaders: { 
        'Authorization': payload,
        'Accept': content_type,
        'Content-Type':content_type,
        'Access-Control-Allow-Origin':'*'
    }
    })
  return request
 }
}
