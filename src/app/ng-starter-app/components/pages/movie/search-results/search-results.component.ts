import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import {PageEvent} from '@angular/material';
import {index, items_per_page, paginate_steps } from './../../../../shared/variables'
import {FormsUtils} from './../../../../shared/utils/utils.service';
import {movie_res_headers, MovieResults, res_types, ResType}  from './../../../../models/movie';
import {Order, List}  from './../../../../models/commons';

@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsComponent {
  @Input() res:MovieResults;
  public readonly types:Array<ResType>=res_types;
  public readonly headers:Array<List>=movie_res_headers;
  public readonly index:number=index;
  public readonly items_per_page:number=items_per_page;
  public readonly paginate_steps:Array<number>=paginate_steps;  
  public filter_type:string;
  public order_by_col:string;
  public toggle_order:boolean;
  public pageEvent: PageEvent;
  constructor( public _router:Router, public _activeRoute:ActivatedRoute, public _formsUtils:FormsUtils) { }
  goTo(payload:Object):Promise<boolean>{
    return this._router.navigate([`./${payload['imdbID']}`], {relativeTo: this._activeRoute});
  }
  order_update(order:Order){
    this.order_by_col=<string>order['by'];
    this.toggle_order=<boolean>order['toggle'];
  }
  setPaginator(payload:PageEvent, custom_page?:number):Object{
    const p={
      pageSize:!!custom_page?custom_page:this.items_per_page,
      pageIndex:this.index      
    }
    if(!!payload){
      return payload
    }else{
      return p
    }
  }
}
