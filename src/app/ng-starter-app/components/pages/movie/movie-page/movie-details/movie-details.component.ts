import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import {MovieExt} from './../../../../../models/movie';

@Component({
  selector: 'movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class MovieDetailsComponent {
@Input() movie:MovieExt;
  constructor() { }
}
