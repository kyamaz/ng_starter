import { Component, OnInit, Directive, AfterViewInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Router, ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {WindowRef} from './../../../../window_ref';   
import { Subject } from 'rxjs/Subject';
import { OmdbService } from './../../../shared/omdb.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  public s_res$= new BehaviorSubject({})
  public datum$=new BehaviorSubject({}); 
  private subscriptions:Subscription[]=[];
  constructor( public _router:Router, protected _winRef:WindowRef, public _omdbService:OmdbService) {}
  ngOnInit():void{
    this.previous_search();
  }
  ngOnDestroy():void{
    this.subscriptions.map(s=>s.unsubscribe());
  }
  logOut():boolean{
    this._winRef.clearLogStorage();
    this._router.navigate(['login']);
    return true;
  }
  setResp(e):void{
    return this.s_res$.next(e);
  }
  previous_search():Subscription{
    const s=this._omdbService.prev_nav$.subscribe(res=>this.datum$.next(res));
    this.subscriptions.push(s);
    return s;
  }
}