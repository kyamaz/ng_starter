import { NgModule } from '@angular/core';
//commons
import { SharedComponentModule } from '../../commons/shared_component.module';
//Component 
import { routing } from './movie.routing';
import {MovieComponent} from './movie-page/movie.component';
import { MovieDetailsComponent } from './movie-page/movie-details/movie-details.component';
import {SearchComponent} from './search.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { SearchResultsComponent } from './search-results/search-results.component';


@NgModule({
  imports: [
    routing,
    SharedComponentModule
  ],
  declarations: [MovieComponent, 
    MovieDetailsComponent,
    SearchComponent,
    SearchInputComponent,
    SearchResultsComponent
  ]
})
export class MoviePageModule { }
