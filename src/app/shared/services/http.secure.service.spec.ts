import { TestBed, inject } from '@angular/core/testing';

import { HttpSecureService } from './http.secure.service';

describe('HttpSecureService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpSecureService]
    });
  });

  it('should be created', inject([HttpSecureService], (service: HttpSecureService) => {
    expect(service).toBeTruthy();
  }));
});
