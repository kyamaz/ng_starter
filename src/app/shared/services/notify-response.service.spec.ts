import { TestBed, inject } from '@angular/core/testing';

import { NotifyResponseService } from './notify-response.service';
import {HttpClientModule} from '@angular/common/http';
import {Router} from '@angular/router';
import {WindowRef} from './../../window_ref';
describe('NotifyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
        HttpClientModule 
      ],
      providers: [NotifyResponseService, 
          WindowRef,
          {provide: Router }
          
      ],
    });
  });

  it('should ...', inject([NotifyResponseService], (service: NotifyResponseService) => {
    expect(service).toBeTruthy();
  }));
});
