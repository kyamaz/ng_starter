import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, Output, EventEmitter, Input, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import {FormsUtils} from './../../../../shared/utils/utils.service';
import {OmdbService} from './../../../../shared/omdb.service';
import { FormGroup, AbstractControl } from '@angular/forms/src/model';
import { distinctUntilChanged, filter } from 'rxjs/operators';
import {Observable}from 'rxjs/Observable';
import {Validators} from "@angular/forms";
import { min_y, max_y } from './../../../../shared/variables';
import { SearchMovie,init_search_form,form_types, MovieResults } from './../../../../models/movie';
import { List } from './../../../../models/commons';

@Component({
  selector: 'search-input',
  templateUrl: './search-input.component.html',
	styleUrls: ['./search-input.component.scss'],
	changeDetection:ChangeDetectionStrategy.OnPush
})
export class SearchInputComponent implements OnInit {
	@Output() queries = new EventEmitter<MovieResults>();
	@Input() datum:SearchMovie;
	constructor( public _formsUtils:FormsUtils, protected _omdbService:OmdbService) { }
	public readonly min_y:number=min_y;
	public readonly max_y:number=max_y;
  private readonly init_form_data:SearchMovie=init_search_form;
  public readonly types:Array<List>=form_types;
	public is_hovered:boolean=false;
	public form:FormGroup;

	ngOnInit(){
		this.init_form();
	}
	undo():void{
		this.form=this._formsUtils.build_form(this.init_form_data);
	}
	init_form():void{
		if(this._formsUtils.objIsEmpty(this.datum)){
			this.form=this._formsUtils.build_form(this.init_form_data);
		}else{
			this.form=this._formsUtils.build_form(this.datum);
			this.query();
			this.form.markAsDirty();	
		}
		this.formValidation();
	}
  query():Subscription{
		const q=this.returnQuery(this.form.value);	
		this._omdbService.prev_nav$.next(this.form.value);
  		return  this._omdbService.loadQuery(q).subscribe(r=>this.queries.emit(r));
	}
	

  returnQuery(obj:SearchMovie):string{
    let q:string="";
    for (let prop in obj){
      if(!!obj[prop]){
        let subq:string;
        subq=prop==='type'?`&type=${obj[prop]}`:`&${prop.charAt(0)}=${obj[prop]}`;
        q += subq
      }
    }
    return q;
	}

	formValidation():void{
		const year_ctrl :AbstractControl=this._formsUtils.getSubForm( this.form, 'year');
		const search_ctrl :AbstractControl=	this._formsUtils.getSubForm( this.form, 'search');
		let arr_ctrl:Array<AbstractControl>=[search_ctrl];
		 year_ctrl.setValidators([
			  Validators.pattern('^[0-9]*$'),
				Validators.min(this.min_y),
				Validators.max(this.max_y)
		]);
		this._formsUtils.setRequireFields(arr_ctrl)
	}
}
