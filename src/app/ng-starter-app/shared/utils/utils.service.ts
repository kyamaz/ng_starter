import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, FormsModule, Validators, FormArray, ValidationErrors } from "@angular/forms";
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { _cloneDeep } from './common.functions.service';
import { Subject } from 'rxjs/Subject';
import { distinctUntilChanged, filter } from 'rxjs/operators';
import {Observable}from 'rxjs/Observable';

class NullControl extends AbstractControl{
    readonly value: any = null;
    readonly pristine: boolean = true;
    readonly errors: ValidationErrors | null = null;
    private static instance: NullControl;
    controls: null = null;

    private fn_counter = {

    }

    setValue(value: any, options?: Object): void {
      this.log("setValue");
      return;
    }
    patchValue(value: any, options?: Object): void {
      this.log("patchValue")
    }
    reset(value?: any, options?: Object): void {
      this.log("Reset")
    }
    
    get(path:any): AbstractControl{
      this.log("get")
      return this;
    }

    push(control: AbstractControl): void{
      this.log('push');
    }

    private log(funcName:string)
    {
      if (!this.fn_counter.hasOwnProperty(funcName)){
        this.fn_counter[funcName] = 0;
      }
      this.fn_counter[funcName] += 1;
      console.error('NullControl', funcName, 'called', this.fn_counter[funcName], 'times');
    }

    static getInstance() {
      if (!NullControl.instance) {
        NullControl.instance = new NullControl(Validators.nullValidator, null);
          // ... any one time initialization goes here ...
      }
      return NullControl.instance;
  }
}


@Injectable()
export class FormsUtils{
  constructor(){}
  build_form(data, select_in_array?:boolean): FormGroup{      
      if (Array.isArray(data)  ){
      let selected_data;
        if(!!select_in_array){
         selected_data=data.map(d=>{
            d.is_selected=false;
            return d;
          })
        }else{
          selected_data=data          
        }
        return new FormGroup({data: this._rec_build_form(selected_data)});
      } else if (this.isObject(data)){
        return <FormGroup>this._rec_build_form(data);
        
      }else{
        return new FormGroup({data: this._rec_build_form(data)});
      }
    }

  _rec_build_form(data){
      if (data instanceof FormArray || data instanceof FormGroup || data instanceof FormControl){
        return data;
      }
      if (Array.isArray(data))
      {
        let array_out = []
        for (let sub of data){
          let output = this._rec_build_form(sub)
          array_out.push(output);
        }
        return new FormArray(array_out)
      } else if (this.isObject(data))
      {
        let copy=_cloneDeep(data);
        for(let pro in copy){
            copy[pro]=this._rec_build_form(copy[pro]);
        }
        return new FormGroup(copy)
      } else
      {
        return new FormControl(data)
      }
    }

     isObject(obj) {
      return obj === Object(obj);
    }

    objIsEmpty(obj:Object):boolean {
      for(let key in obj) {
          if(obj.hasOwnProperty(key))
              return false;
      }
      return true;
    }
    
    getSubFormByList<T extends AbstractControl>( form:AbstractControl, path: string[]): T | NullControl{
      let t: AbstractControl;
      t = form;
      let accu:Array<string> = [];
      path.map( p=>{
        let old_t = t;
        t = t.get(p);
        if (!t){
          if (accu.length != 0){
            console.error(p, "not found after", accu.join('->'));
          }else
          {
            console.error(p, "not found at root");
          }
          console.error('possible choices are:');
          if (old_t instanceof FormControl){
            console.error('\t', old_t.value)
          }else if (old_t instanceof FormGroup){
            for (let key in old_t.value){
              console.error('\t', key);
            }
          }
          else if (old_t instanceof FormArray){
            console.error('any integer between 0 and ', old_t.value.length - 1);
          }else
          {
            console.error('\t no choice')
          }
          t = NullControl.getInstance();
        }
        accu.push(p)
      });
      return (<T>t);
    }
  
    getSubForm<T extends AbstractControl>( form:AbstractControl, ...path: string[]): T | NullControl{
      return this.getSubFormByList(form, path);
    }
  
  //validators
  setRequireFields( ctrls:Array<AbstractControl>):void{
		ctrls.map( ctrl=> ctrl.setValidators([ Validators.required ]));  
	}
	toggleRequiredFiels(src_ctrl:AbstractControl, dest_ctrl:AbstractControl){
		src_ctrl.valueChanges
		.pipe( distinctUntilChanged() )
		.subscribe(
			val=>{
				if(!!val && val.length>0){
					dest_ctrl.setValidators([]);
					dest_ctrl.updateValueAndValidity();
				}
			}
		)
	}
}

