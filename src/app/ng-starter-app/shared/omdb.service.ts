import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {api_key} from './variables';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class OmdbService {
  public prev_nav$=new BehaviorSubject({});

  constructor(private _http: HttpClient) { }

  loadQuery(query) :Observable<any> {
    return this._http.get(`http://www.omdbapi.com/?apikey=${api_key}${query}`);
  }
}
