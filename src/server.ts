import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import { renderModuleFactory } from '@angular/platform-server';
import { enableProdMode } from '@angular/core';
import * as express from 'express';
import { join } from 'path';
import { readFileSync } from 'fs';
import * as http from "http";
import * as bodyParser from 'body-parser';
import * as path from 'path';
import { Response, Request } from 'express';

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

export class Server {
    public app:any;
    public server: any;
    public port: number;
    public http:any;
    private session_token:string;
    private readonly pwd:string="root";
    private readonly login:string="test";
   // Bootstrap the application.
    public static bootstrap(): Server {
        return new Server();
    }
    constructor() {
        this.http=http;
        enableProdMode();

        // Create expressjs application
        this.app = express();

        // Configure application
        this.config();
        
        // Setup routes
          this.routes();

        // Create server
        this.server =  this.http.createServer(this.app);

        // Start listening
        this.listen();
    }

    // Configuration
  private config(): void {
        // By default the port should be 4900
        this.port = parseInt(process.env.PORT) || 4900;
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
 
    // Configure routes
  private routes(): void {
    const router: express.Router = express.Router();
    const DIST_FOLDER:string = join(process.cwd(), 'dist');
    // Our index.html we'll use as our template
    const template:string = readFileSync(join(DIST_FOLDER, 'browser', 'index.html')).toString();
    // * NOTE :: leave this as require() since this file is built Dynamically from webpack
    const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./../dist/server/main.bundle');
    const { provideModuleMap } = require('@nguniversal/module-map-ngfactory-loader');

    this.app.engine('html', (_, options, callback) => {
      renderModuleFactory(AppServerModuleNgFactory, {
        // Our index.html
        document: template,
        url: options.req.url,
        // DI so that we can get lazy-loading to work differently (since we need it to just instantly render it)
        extraProviders: [
          provideModuleMap(LAZY_MODULE_MAP)
        ]
      }).then(html => {
        callback(null, html);
      });
    });
    this.app.set('view engine', 'html');
    this.app.set('views', join(DIST_FOLDER, 'browser'));
    
    // Server static files from /browser
    this.app.get('*.*', express.static(join(DIST_FOLDER, 'browser')));
    // authentification
     this.app.use('*',  (req:Request, res:Response, next:Function)=> {
      const end_point=req.baseUrl;
      if ( end_point === '/login' || req.method === 'POST' && end_point=== '/log') {
        return next();
      }
      if (!!!req['headers']['authorization']){
        return unauthorizedRequest(req, res);
       }else{
         if(req['headers']['authorization']=== this.session_token){
            return next();
         }
       }
      return  unauthorizedRequest(req, res);
      
    }); 
    //API end point
    this.app.post("/log",(req:Request, res:Response):Response=>{
      const body:Request['body']=req.body;
      if( body.username===this.login &&  body.password===this.pwd){
        this.session_token=Math.random().toString(36).substring(3);
        res.status(200).json({session_token: this.session_token});
      }else{
        return  unauthorizedRequest(req, res);
      }
    });
    this.app.get("/auth", (req:Request, res:Response)=>res.end())
    // All regular routes use the Universal engine
    this.app.get('*', (req:Request, res:Response) :Response => {
      //res.sendFile(path.join( DIST_FOLDER, './browser/index.html')); 
      res.render(join(DIST_FOLDER, 'browser', 'index.html'), { req });
      return res;
    });

    function unauthorizedRequest(req:Request, res:Response):Response{
      res.statusCode = 401;
      //res.sendFile(path.join( DIST_FOLDER, './browser/index.html')); 
      res.render(join(DIST_FOLDER, 'browser', 'index.html'), { req });
      return res
    }
  }
 

        // Start HTTP server listening
    private listen(): void {
        //listen on provided ports
        this.server.listen(this.port);
       //add error handler
        this.server.on("error", error => {
            console.log("ERROR", error);
        });
        //start listening on port
        this.server.on("listening", () => {
            console.log('==> Listening on port %s. Open up http://localhost:%s/ in your browser.', this.port);            
        });
    }
  
}
// Bootstrap the server
Server.bootstrap();

