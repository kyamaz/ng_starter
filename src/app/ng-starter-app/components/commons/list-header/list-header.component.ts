import { Component, OnInit, Input, Output, EventEmitter , ChangeDetectionStrategy} from '@angular/core';
import {Order}  from './../../../models/commons';

@Component({
  selector: 'list-header',
  templateUrl: './list-header.component.html',
  styleUrls: ['./list-header.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class ListHeaderComponent {
  @Input() headers:Array<Object>=[];
  @Output() orderUpdate = new EventEmitter<Order>();
  public toggle_order:boolean;
  public last_order:string;
  constructor() { }

  orderBy(term):void{
    let order={
      by:term,
      toggle: undefined
    }
    if(term.value==this.last_order){
        this.toggle_order=!this.toggle_order;
        order.toggle=this.toggle_order;
        return this.orderUpdate.emit(order);
    }else{
      this.toggle_order=true;
      order.toggle=this.toggle_order;
      this.last_order=term.value;
      return  this.orderUpdate.emit(order);
    }
  }
}
