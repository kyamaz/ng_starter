import { SharedComponentModule } from './../../commons/shared_component.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './redirect.routing';
import { RedirectComponent } from './redirect.component';

@NgModule({
  imports: [
    CommonModule,
    SharedComponentModule,
    routing
  ],
  declarations: [
    RedirectComponent
  ]
})
export class RedirectPagesModule { }
