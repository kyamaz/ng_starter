import { ActivatedRoute, Router } from '@angular/router';
import { OmdbService } from './../../../../shared/omdb.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import {MovieExt} from './../../../../models/movie';
@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit, OnDestroy {

  constructor( protected _omdbService:OmdbService, public _activateRoute:ActivatedRoute, public _router:Router ) { }
  public datum$= new BehaviorSubject({});
  public subscriptions:Subscription[]=[];
  ngOnInit():void{
   this.loadMovie();
  }
  ngOnDestroy():void{
    this.subscriptions.map(s=>s.unsubscribe());
  }

  loadMovie():Subscription{
   const s=this._activateRoute
            .params
            .subscribe(params => {
                if(!!params){
                  const q=`&i=${params.id}`;
                  this._omdbService.loadQuery(q).subscribe(r=>{
                    if(r.Response==="False"){
                        this._router.navigate(['/404']);
                    }else{
                      this.datum$.next(r)
                    }
                  })
                }
            });
    this.subscriptions.push(s);
    return s;
  }

}
