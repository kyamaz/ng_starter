export const api_key="YOUR API KEY"
export const index:number=0;
export const items_per_page:number=15;
export const paginate_steps:Array<number>=[5,10, 15];  
export const min_y=1900;
export const max_y=new Date().getFullYear()+5;
