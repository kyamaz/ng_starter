import { NgModule } from '@angular/core';
import {RouterModule, Routes, Resolve, ActivatedRouteSnapshot,RouterStateSnapshot } from "@angular/router"
import {Injectable} from "@angular/core"
import {Observable} from 'rxjs/Observable'
//route protection
import { RouteGuard,LeaveAppGuard } from './routes.guard';
// component
import { LoginComponent } from '../../login-page/login/login.component';

//trash
 import { RedirectComponent } from './../../ng-starter-app/components/pages/redirect/redirect.component';

 export const appRoutes: Routes = [
    { path: 'login', component: LoginComponent},
    { path: 'movie', 
      canActivate: [RouteGuard],
      loadChildren: './../../ng-starter-app/components/pages/movie/movie.pages.module#MoviePageModule'
    },
    { path:'404',
      component: RedirectComponent,
      canActivate: [RouteGuard]
    },
    { path: '**', redirectTo: '/404',pathMatch: 'full'}
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class RoutesRoutingModule { }