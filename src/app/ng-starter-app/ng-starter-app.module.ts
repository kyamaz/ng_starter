import { NgModule } from '@angular/core';
import {PagesModule} from './components/pages/pages.module';
@NgModule({
    imports: [
      PagesModule,
    ]
    })
export class NgStarterAppModule { }