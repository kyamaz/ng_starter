import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Router} from "@angular/router";
import {NotifyResponseService} from './notify-response.service';
import { AuthService } from './auth/auth.service';
import {AuthUtilsService } from './auth/auth.utils.service';
import {WindowRef} from './../../window_ref';   
import { tap, delay } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Subscription } from 'rxjs/Subscription';
@Injectable()
export class HttpSecureService implements HttpInterceptor {
  private authService;
  private accept_content:string='application/json;charset=UTF-8';
  constructor( private _router: Router,
               private NotifyResponseService :NotifyResponseService,
               private winRef :WindowRef,
              private auth_utils:AuthUtilsService) {
  }


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    //set req headers
    if(!!this.winRef.nativeWindow.localStorage.getItem('session_token')){
      const session:string=this.winRef.nativeWindow.localStorage.getItem('session_token');
      if(!request.url.includes(("omdbapi"))){
          request= this.auth_utils.setHeaderClient(request, session, this.accept_content)
        }      
      }
    
      return next.handle(request)
      .pipe(
        tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            const m={
              status:200,
              statusText:'loaded'
            }
            this.NotifyResponseService.notify(m);
            this.removeNotif(1500);
          }
          },
          (err: any) => {
            if (err instanceof HttpErrorResponse) {
             // console.log(err)
              if (err.status === 401) {
                this.winRef.clearLogStorage();
                this._router.navigate(['login']);
              }
              if (err.status === 404) {
                this._router.navigate(['/404']);
              }
              this.NotifyResponseService.notify(err.message);
              this.removeNotif(3000);

            }
          }
        )
      )
    }
    private removeNotif(time:number):Subscription{
      let timer=  of({}).pipe(
        delay(time)
      ).subscribe(r=> this.NotifyResponseService.notify(r));
      return timer;
    }
}