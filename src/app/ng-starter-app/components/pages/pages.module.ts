
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
//components
//pages
import {MoviePageModule} from './movie/movie.pages.module';
import { RedirectPagesModule } from './redirect/redirect.pages.module';
  //commons
import { SharedComponentModule } from './../commons/shared_component.module';
//service
import {FormsUtils} from "./../../shared/utils/utils.service";
import {CommonFunctionsService} from './../../shared/utils/common.functions.service';
import {OmdbService} from './../../shared/omdb.service';

@NgModule({

  imports: [
     CommonModule,
     RouterModule,
     SharedComponentModule,
     MoviePageModule,
     RedirectPagesModule
    ],

  providers: [
      FormsUtils, 
      CommonFunctionsService, 
      OmdbService
  ]
})
export class PagesModule { }
