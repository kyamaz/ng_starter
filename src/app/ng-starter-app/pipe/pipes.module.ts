import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginatePipe } from './paginate.pipe';
import {OrderByPipe} from './order_by.pipe';
import {FilterPipe} from './filter.pipe';

@NgModule({
  imports:[ CommonModule],
  declarations: [ 
    OrderByPipe,
    PaginatePipe,
    FilterPipe
  ],
  exports: [ 
    PaginatePipe,
    OrderByPipe,
    FilterPipe
  ]
})
export class PipesModule { }
