import { Router,ActivatedRoute, NavigationEnd  } from '@angular/router';
import { Component,OnInit, Inject,ViewEncapsulation } from '@angular/core';
import {WindowRef} from './window_ref';   
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {NotifyResponseService} from './shared/services/notify-response.service';
import {fadeInAnimation} from "./ng-starter-app/shared/animations/animations";
import {  filter } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls:['./app.component.scss'],
  encapsulation:ViewEncapsulation.None,
  animations: [fadeInAnimation]  
})
export class AppComponent implements OnInit {
  public server$=new BehaviorSubject ({});  
  constructor( private _router:Router, private winRef: WindowRef, 
              private _notifyResponseService :NotifyResponseService, private route: ActivatedRoute){}
  ngOnInit() {
    this.subServerResp();
  }
  subServerResp():Subscription{
    return this._notifyResponseService.server_response$.subscribe(r=>this.server$.next(r))
  }
  dismiss_notif():void{
    return this.server$.next({});
  }
  getRouteAnimation(outlet):any{
    return outlet.activatedRouteData.animation
  }
}
