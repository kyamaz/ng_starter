
import { AppComponent } from './app.component';
//vendor modules
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable, Injector} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

//services
import { AuthService } from './shared/services/auth/auth.service';
import {AuthUtilsService} from './shared/services/auth/auth.utils.service';
import {HttpSecureService} from './shared/services/http.secure.service';
import {NotifyResponseService} from "./shared/services/notify-response.service";
//app modules
import{NgStarterAppModule} from './ng-starter-app/ng-starter-app.module';
import{LoginModule} from './login-page/login.module';
import { SharedComponentModule } from './ng-starter-app/components/commons/shared_component.module';
// routes 
import { RoutesRoutingModule } from './shared/routes/routes-routing.module';
import { RouterModule, Routes, Router } from '@angular/router';
import { RouteGuard, LeaveAppGuard } from './shared/routes/routes.guard';
import {WindowRef} from './window_ref';
//sw
import { ServiceWorkerModule } from '@angular/service-worker'
import { environment } from 'environments/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    CommonModule,
    RoutesRoutingModule,
    NgStarterAppModule,
    LoginModule,
    BrowserAnimationsModule,
    SharedComponentModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled:environment.production})    
  ],
    exports: [ 
      
    ],
  providers: [
    AuthService,
    RouteGuard,
    LeaveAppGuard,
    NotifyResponseService,
    WindowRef,
    AuthUtilsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpSecureService,
      multi: true
    }
],

  bootstrap: [AppComponent]
})
export class AppModule { }
