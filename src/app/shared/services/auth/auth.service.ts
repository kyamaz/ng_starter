import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Injectable,Inject, PLATFORM_ID } from '@angular/core';
import { Router } from '@angular/router';
import {WindowRef} from './../../../window_ref';   
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Injectable()
export class AuthService{
  public is_authorized$ =new BehaviorSubject({});
  public readonly  url:string=this.winRef.getUrl();
  constructor(private http:HttpClient, private _router:Router, private winRef: WindowRef, @Inject(PLATFORM_ID) private platformId: Object) {}
  authenticate(login){

    this.http.post(`${this.url}/log`,login )
    .subscribe(res=>{
      this.is_authorized$.next(res);
      this._router.navigate(['movie']);
    })

  }

  checkAuthentication(){
    return  this.http.get(`${this.url}/auth`);
  }


  clearAuthentication():Subscription{
    return this.http.get(`${this.url}/logout`).subscribe(
      res=>{
        if(res['status']===200){          
          if (isPlatformBrowser(this.platformId)) {
            if(!!this.winRef.nativeWindow.localStorage.getItem('session_token')){
              this.winRef.nativeWindow.localStorage.removeItem('session_token');
              this.winRef.nativeWindow.localStorage.removeItem('roles');
              }
          }
        }
        this._router.navigate(['login']);
      })   
    }
  }


