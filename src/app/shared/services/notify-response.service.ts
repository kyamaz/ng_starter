import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {Router} from '@angular/router';
import {WindowRef} from '../../window_ref';   

@Injectable()
export class NotifyResponseService {
  public server_response$= new Subject();
  constructor( private _router:Router, private winRef :WindowRef ) {}
  notify(data):void{
    this.server_response$.next(data);
  }
}
