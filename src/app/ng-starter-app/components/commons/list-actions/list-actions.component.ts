import { Component, OnInit,ChangeDetectionStrategy, Output, Input, EventEmitter, OnDestroy } from '@angular/core';

@Component({
  selector: 'list-actions',
  templateUrl: './list-actions.component.html',
  styleUrls: ['./list-actions.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class ListActionsComponent {
  @Input() types:Array<Object>;
  @Output() filterByUpdate = new EventEmitter();
  public filterBy:string;
  constructor() { }
  newfilterByTypeSelection(payload):void{
    this.filterByUpdate.emit(payload);
  }
}
