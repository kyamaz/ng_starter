import { AuthService } from './../../shared/services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder,  Validators } from '@angular/forms';
import {Router, NavigationStart} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'api/login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent{
    public form:FormGroup;
    public failed:boolean=false;
    private delay_form_submit: Observable<any>;
    public sending:boolean=false;
    constructor( _fb:FormBuilder, private _authService :AuthService, private _router:Router) {
        this.form = _fb.group({
            username: ['', Validators.compose([
                Validators.required,
                Validators.minLength(2)
            ])
            ],
                password: ['', Validators.compose([
                Validators.required,
                Validators.minLength(2)
            ])
            ]
        });    

    this.delay_form_submit = new Observable(observer => {
          setTimeout(() => {
              observer.next('delay over');
          }, 1000);
      });
        
    }
 onSubmit(login=this.form.value):Subscription {
     this.sending=true;
    this.form.disable()
    return this.delay_form_submit.subscribe(
        delay=> {
            this._authService.authenticate(login)
            this.form.enable();
            this.sending=false;
            this.failed=true
            this.delay_form_submit.subscribe( _=> this.failed=false);
        },
        err=>console.log('err'),
    )                
 }
}

