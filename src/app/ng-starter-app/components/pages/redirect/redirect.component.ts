import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class RedirectComponent {
  constructor(protected _router:Router, protected _location:Location) { }
  goBack(){
    this._location.back();
  }
  goHome(){
    this._router.navigate(['movie']);
  }

}
